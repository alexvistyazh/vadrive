# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-06-28 16:13
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trash', '0002_trashmodel_time_creation'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trashmodeltask',
            old_name='basket',
            new_name='trash',
        ),
    ]
