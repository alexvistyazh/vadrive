from django.conf.urls import url
from trash import views

urlpatterns = [
    url(r'^create/$', views.CreateTrashView.as_view(), name='create_trash'),
    url(r'^$', views.trash_list, name='trash_list'),
    url(r'^content/(?P<trash_name>\w+)/$', views.trash_content, name='trash_content'),
    url(r'^remove_ajax/$', views.remove_ajax, name='remove_files'),
    url(r'^remove_form/$', views.RemoveFormView.as_view(), name='remove'),
    url(r'^recover_form/$', views.RecoveryFormView.as_view(), name='recover'),
    url(r'^recover_ajax/$', views.recover_ajax, name='recover_files'),
    url(r'^content_ajax/$', views.trash_content_ajax, name='trash_content_ajax'),
    url(r'^tasks/$', views.tasks, name='tasks'),
]
