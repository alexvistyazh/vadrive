from django.test import TestCase
from trash.models import TrashModel, TrashModelTask
from varm.trash.trash import DEFAULT_SIZE_LIMIT, DEFAULT_FILES_LIMIT
import tempfile
import os
import shutil


class ModelTestCase(TestCase):

    BASKET_NAME = 'TEST_BASKET'
    BASKET_SIZE = DEFAULT_SIZE_LIMIT
    BASKET_FILES = DEFAULT_FILES_LIMIT

    def setUp(self):
        self.test_file = tempfile.NamedTemporaryFile(delete=False)
        self.test_file_basename = os.path.basename(self.test_file.name)

        self.trash = TrashModel.objects.create(
            name=self.BASKET_NAME,
            files_limit=self.BASKET_FILES,
            size_limit=self.BASKET_SIZE,
        )

        self.trash.instance.insert(self.test_file.name)

    def test_remove(self):
        self.assertTrue(self.test_file_basename in self.trash.instance)

        self.trash.remove(self.test_file_basename)
        self.assertTrue(self.test_file_basename not in self.trash.instance)

        self.assertTrue(
            self.trash.trashmodeltask_set.filter(
                operation=TrashModelTask.OPERATION_REMOVE
            ).exists()
        )

    def test_recover(self):
        self.assertTrue(self.test_file_basename in self.trash.instance)

        self.trash.recover(self.test_file_basename)

        self.assertTrue(os.path.exists(self.test_file.name))

        self.assertTrue(
            self.trash.trashmodeltask_set.filter(
                operation=TrashModelTask.OPERATION_RECOVER
            ).exists()
        )

    def tearDown(self):
        shutil.rmtree(self.trash.instance.path)
        self.trash.trashmodeltask_set.all().delete()
        self.trash.delete()
