from django import forms
from trash.models import TrashModel


class CreateTrashForm(forms.ModelForm):
    """
    Model-based form for create trash.
    """
    class Meta:
        model = TrashModel
        fields = ['name', 'files_limit', 'size_limit']


class TaskForm(forms.Form):
    """
    Task form.
    """
    trash = forms.CharField(max_length=50)
    file = forms.CharField(max_length=50)


class DynamicTaskForm(forms.Form):
    """
    Dynamic form for tasks.
    Fields
        trash - store trash list from TrashModel
        file - store list files from trash
    """
    trash = forms.ModelChoiceField(queryset=TrashModel.objects.all())
    file = forms.ChoiceField()
