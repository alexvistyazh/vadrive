function update_select_component(options) {
    var select_component = $(".modal #id_file");

    select_component.empty();
    for (var j = 0; j < options.length; ++j) {
        select_component.append($('<option>').val(options[j]).html(options[j]));
    }
}

function update_trash_file_list(trash_name) {
    $.ajax({
        url: '/content_ajax/',
        type: 'POST',
        data: {
            'trash': trash_name,
            'csrfmiddlewaretoken': getCookie('csrftoken')
        },
        success: function (data) {
            update_select_component(data.files);
        },
        error: function () {
            update_select_component([]);
        }
    });
}

$(document).ready(function () {
    $(".modal #id_trash").change(function () {
        var trash_name = $(this).find('option:selected').text();
        update_trash_file_list(trash_name);
    });
});
