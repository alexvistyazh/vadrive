function chosen_files() {
    var result = [];
    $('.file').each(function (i, obj) {
        if (obj.checked){
            result.push(obj.name);
        }
    });
    return result;
}

function trash_name() {
    var url_tokens = location.href.split('/');
    return url_tokens[url_tokens.length - 2];
}

function send_request(url) {
    $.ajax({
        url: url,
        type: 'POST',
        data: {
            'trash': trash_name(),
            'files': JSON.stringify(chosen_files()),
            'csrfmiddlewaretoken': getCookie('csrftoken')
        }
    });
}

$(document).ready(function () {
    $('#removed').click(function () {
       send_request('/remove_ajax/');
    });

    $('#recovered').click(function () {
        send_request('/recover_ajax/');
    });
});
