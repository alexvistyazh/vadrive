from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import FormView
from django.http import JsonResponse
from trash.forms import CreateTrashForm, DynamicTaskForm, TaskForm
from trash.models import TrashModel, TrashModelTask
from json import loads


class CreateTrashView(FormView):
    """
    Class-based form processing view, which create trash by name,
    files_limit and size_limit form form fields.
    """
    form_class = CreateTrashForm
    success_url = '/'

    def form_valid(self, form):
        """
        Override action on not form.is_valid
        :param form: filled instance of form_class
        :return: form valid of super class
        """
        TrashModel.objects.create(
            name=form.data.get('name'),
            files_limit=form.data.get('files_limit'),
            size_limit=form.data.get('size_limit'),
        )
        return super(CreateTrashView, self).form_valid(form)


class RemoveFormView(FormView):
    """
    Class-based form processing view, which remove file from trash by
    trash name and trash file from form fields.
    """
    form_class = TaskForm
    success_url = '/tasks/'

    def form_valid(self, form):
        """
        Override action on form.is_valid
        :param form: filled instance of form_class
        :return: form_valid of super class
        """
        trash = get_object_or_404(TrashModel, pk=form.data.get('trash'))
        trash.remove(form.data.get('file'))
        return super(RemoveFormView, self).form_valid(form)


class RecoveryFormView(FormView):
    """
    Classed-based form processing view, which recover file from trash by
    trash and trash file from form fields.
    """
    form_class = TaskForm
    success_url = '/tasks/'

    def form_invalid(self, form):
        print form

    def form_valid(self, form):
        """
        Override action on form.is_valid
        :param form: filled instance of form_class
        :return: form_valid of super class
        """
        trash = get_object_or_404(TrashModel, pk=form.data.get('trash'))
        trash.recover(form.data.get('file'))
        return super(RecoveryFormView, self).form_valid(form)


def trash_list(request):
    """
    View trash list page
    :param request: django HttpRequest
    :return: page with trash list of trashes and trash menu
    """
    return render(request, 'trash_list.html', {
        'form': CreateTrashForm(),
        'trashes': TrashModel.objects.all(),
    })


def trash_content(request, trash_name):
    """
    View trash content page
    :param request: django HttpRequest
    :param trash_name: name of trash
    :return page with trash content page with trash files and trash menu
    """
    return render(request, 'trash_content.html', {
        'trash_name': trash_name,
        'content': get_object_or_404(TrashModel, name=trash_name).content()
    })


def tasks(request):
    """
    View task page
    :param request: django HttpRequest
    :return: page with task page with task history and task menu
    """
    return render(request, 'tasks.html', {
        'form': DynamicTaskForm(),
        'tasks': TrashModelTask.objects.all()
    })


def remove_ajax(request):
    """
    Remove from trash some list of files from ajax query
    :param request: django HttpRequest
    :return: json with ok or bad request status
    """
    if request.is_ajax() and request.method == 'POST':
        trash_name = request.POST.get('trash', None)
        files = loads(request.POST.get('files', '[]'))
        get_object_or_404(TrashModel, name=trash_name).remove_many(files)
        return JsonResponse({'status': 'method remove was called'})
    else:
        return JsonResponse({'status': 'bad request'})


def recover_ajax(request):
    """
    Recover from trash some list of files by ajax query
    :param request: django HttpRequest
    :return: json with ok or bad request status
    """
    if request.is_ajax() and request.method == 'POST':
        trash_name = request.POST.get('trash', None)
        files = loads(request.POST.get('files', '[]'))
        get_object_or_404(TrashModel, name=trash_name).recover_many(files)
        return JsonResponse({'status': 'method recover was called'})
    else:
        return JsonResponse({'status': 'bad request'})


def trash_content_ajax(request):
    """
    Get trash content by trash id by ajax query
    :param request: django HttpRequest
    :return: json status ok or bad request
    """
    if request.is_ajax() and request.method == 'POST':
        trash_name = request.POST.get('trash', None)
        trash = get_object_or_404(TrashModel, name=trash_name)
        return JsonResponse({
            'files': [name for name, size, date in trash.content()]
        })
    else:
        return JsonResponse({})
