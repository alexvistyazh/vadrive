from __future__ import unicode_literals
from django.db import models
from varm.trash.trash import Trash, DEFAULT_FILES_LIMIT, DEFAULT_SIZE_LIMIT
from os.path import join, expanduser
from multiprocessing.pool import ThreadPool


class TrashModel(models.Model):
    """
    TrashModel store settings for instance of pyrm.trash.trash.Trash class
    Fields:
        name - name of pyrm.trash.trash.Trash
        files_limit - files_limit of pyrm.trash.trash.Trash
        size_limit - size_limit of pyrm.trash.trash.Trash
        time_creation - time of creation TrashModel instance
    """

    ROOT_PATH = expanduser('~')

    name = models.CharField(max_length=50, unique=True)
    files_limit = models.PositiveIntegerField(default=DEFAULT_FILES_LIMIT)
    size_limit = models.PositiveIntegerField(default=DEFAULT_SIZE_LIMIT)
    time_creation = models.DateTimeField(auto_now=True)

    def remove(self, filename):
        """
        Remove file from trash by filename
        :param filename: name file, which will removed
        :return: None
        """
        removed_files = self.instance.remove(filename)
        status = TrashModelTask.STATUS_SUCCESS if removed_files \
            else TrashModelTask.STATUS_FAIL

        self.trashmodeltask_set.create(
            status=status,
            operation=TrashModelTask.OPERATION_REMOVE,
            filename=filename,
        )

    def recover(self, filename):
        """
        Recover file from trash by filename
        :param filename: name of file which will removed
        :return: None
        """
        recovered_files = self.instance.recover(filename)
        status = TrashModelTask.STATUS_SUCCESS if recovered_files \
            else TrashModelTask.STATUS_FAIL

        self.trashmodeltask_set.create(
            status=status,
            operation=TrashModelTask.OPERATION_RECOVER,
            filename=filename,
        )

    def remove_many(self, file_list):
        """
        Remove list of files. Each file remove in new thread
        :param file_list: list of file which will remov
        :return: None
        """
        pool = ThreadPool()
        for file in file_list:
            pool.apply_async(self.remove, args=(file,))

        pool.close()
        pool.join()

    def recover_many(self, file_list):
        """
        Recover list of files. Each file recover in new thread
        :param file_list: list of file which will recover
        :return: None
        """
        pool = ThreadPool()
        for file in file_list:
            pool.apply_async(self.recover, args=(file,))

        pool.close()
        pool.join()

    def content(self):
        """
        Trash content
        :return: list of trash files
        """
        return self.instance.list()

    @property
    def instance(self):
        """
        Trash instance property
        :return: pyrm.trash.trash.Trash instance with model settings
        """
        return Trash(
            join(self.ROOT_PATH, self.name),
            self.files_limit,
            self.size_limit
        )

    def __str__(self):
        return self.name


class TrashModelTask(models.Model):
    """
    Store detail info for each task of trash model.
    Fields:
        trash - target of task
        status - store trash task status based on STATUS choices
        operation - store trash task operation
        filename - target of task based on OPERATIONS choices
    """
    STATUS_SUCCESS = 0
    STATUS_FAIL = 1

    STATUS = (
        (STATUS_SUCCESS, 'Success'),
        (STATUS_FAIL, 'Fail'),
    )

    OPERATION_REMOVE = 0
    OPERATION_RECOVER = 1

    OPERATIONS = (
        (OPERATION_REMOVE, 'Remove'),
        (OPERATION_RECOVER, 'Recover'),
    )

    trash = models.ForeignKey(TrashModel, on_delete=models.CASCADE)
    status = models.IntegerField(choices=STATUS)
    operation = models.IntegerField(choices=OPERATIONS)
    filename = models.FilePathField()

    def __str__(self):
        return 'Query {0}'.format(self.pk)
