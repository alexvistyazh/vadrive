# PYDRIVE #
Web interface for [varm](https://bitbucket.org/alexvistyazh/vistyazhrm).
Based on framework [Django](https://www.djangoproject.com/).

## PROJECT STRUCTURE ##
* app trash:
    * models.py - models of app trash
    * views.py - views of app trash
    * urls.py - urls of app trash
    * forms.py - forms of app trash
    * templates - html templates of app trash

## INSTALL ##
Install virtual env:

`sudo apt-get install virtualenv`

Create virtual env:

`virtualenv env`

Activate virtual env:

`source env/bin/activate`

Download project:

`git clone git@bitbucket.org:alexvistyazh/vadrive.git`

Install dependencies:

`pip install -r pydrive/requirements.txt`

Run server:

`python pydrive/manage.py runserver`

Deactivate virtual env:

`deactivate`